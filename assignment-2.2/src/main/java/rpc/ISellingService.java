package rpc;

import java.rmi.Remote;
import java.rmi.RemoteException;


public interface ISellingService extends Remote{
  
	/**
	 * Computes the tax to be payed for a Car.
	 *
	 * @param c Car for which to compute the tax
	 * @return price for the car
	 */
	double computeSellingPrice(Car c) throws RemoteException;
	double computeTax(Car c)throws RemoteException;
}

