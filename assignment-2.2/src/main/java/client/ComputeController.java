package client;


import javax.swing.*;

import rpc.Car;
import rpc.ISellingService;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *  Controller for the interface elements of the client.
 */
public class ComputeController {
	//private static final Log LOGGER = LogFactory.getLog(ComputeController.class);

	private ComputeView computeView;

	public ComputeController() {
		computeView = new ComputeView();
		computeView.setVisible(true);
		//serverConnection = new ServerConnection(HOST, PORT);

		computeView.addBtnTaxActionListener(new TaxActionListener());
		computeView.addBtnPriceActionListener(new PriceActionListener());
	}

	public void printCar(double d) {
		if (d != 0) {
			computeView.printCar(d);
		}
	}

	public void displayErrorMessage(String message) {
		computeView.clear();
		JOptionPane.showMessageDialog(computeView, message, "Error", JOptionPane.ERROR_MESSAGE);
	}

	public void displayInfoMessage(String message) {
		computeView.clear();
		JOptionPane.showMessageDialog(computeView, message, "Success", JOptionPane.PLAIN_MESSAGE);
	}


	class TaxActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {  
			          // Getting the registry 
			        Registry registry = LocateRegistry.getRegistry(null); 
			     
			          // Looking up the registry for the remote object 
			        ISellingService stub = (ISellingService) registry.lookup("ISellingService"); 
			     
			      	int year = Integer.parseInt(computeView.getYear());
					int engineCapacity = Integer.parseInt(computeView.getEngineCapacity());
					double purchasingPrice = Double.parseDouble(computeView.getPurchasingPrice());
					
					printCar(stub.computeTax(new Car(year,engineCapacity,purchasingPrice)));
					System.out.print(stub.computeTax(new Car(year,engineCapacity,purchasingPrice)));}
			          
			          // Calling the remote method using the obtained object 
			          //stub.printMsg(); 
			          
			
			 catch (NumberFormatException ex) {
				displayErrorMessage("Please enter values!");
			} 
			          // System.out.println("Remote method invoked"); 
			catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
}
}

	
	class PriceActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			try {  
		          // Getting the registry 
		          Registry registry = LocateRegistry.getRegistry(null); 
		     
		          // Looking up the registry for the remote object 
		          ISellingService stub = (ISellingService) registry.lookup("ISellingService"); 
		     
		      	int year = Integer.parseInt(computeView.getYear());
				int engineCapacity = Integer.parseInt(computeView.getEngineCapacity());
				double purchasingPrice = Double.parseDouble(computeView.getPurchasingPrice());
				
				printCar(stub.computeSellingPrice(new Car(year,engineCapacity,purchasingPrice)));
				System.out.print(stub.computeSellingPrice(new Car(year,engineCapacity,purchasingPrice)));}
		          
		          // Calling the remote method using the obtained object 
		          //stub.printMsg(); 
		          
		
		 catch (NumberFormatException ex) {
			displayErrorMessage("Please enter values!");
		} 
		          // System.out.println("Remote method invoked"); 
		catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		}
	}
}


	


