package main.java.entities;

import com.rabbitmq.client.Consumer;
import main.java.utils.MailService;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.SerializationUtils;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;

public class ConsumerClass extends EndPoint implements Runnable, Consumer {


    public ConsumerClass(String endpointName) throws IOException {
        super(endpointName);
    }

    public static void sendMail(Object message) {
        MailService mailService = new MailService("valea.ovidiu@gmail.com", "ywuvtphdosjqlxzb");
        //System.out.println("The new dvd is " + message. getTitle());
        mailService.sendMail("valea.ovidiu@gmail.com", "Assignment 3.2", message.toString());
    }


    public void run() {
        try {
            //start consuming messages. Auto acknowledge messages.
            channel.basicConsume(endPointName, true, this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Called when consumer is registered.
     */
    public void handleConsumeOk(String consumerTag) {
        System.out.println("ConsumerClass " + consumerTag + " registered");
    }

    /**
     * Called when new message is available.
     */
    public void handleDelivery(String consumerTag, Envelope env,
                               BasicProperties props, byte[] body) throws IOException {
        Dvd dvd = (Dvd) SerializationUtils.deserialize(body);
        System.out.println("Message Number " + dvd.getTitle() + " received.");

    }

    public void handleCancel(String consumerTag) {
    }

    public void handleCancelOk(String consumerTag) {
    }

    public void handleRecoverOk(String consumerTag) {
    }

    public void handleShutdownSignal(String consumerTag, ShutdownSignalException arg1) {
    }
}
