package main.java.entities;

import java.io.IOException;

public class ConsumerStart {

    public static void main(String[] args) throws IOException {

        ConsumerClass consumer = new ConsumerClass("queue");
        Thread consumerThread = new Thread(consumer);
        consumerThread.start();
    }
}
