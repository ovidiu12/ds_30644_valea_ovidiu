package main.java.utils;

import main.java.entities.Dvd;

import java.io.*;

public class TextDocumentService {

    public static void saveToTextDocument(Dvd message) throws IOException {
        File file = new File("W:/LabDS/Assignment3.2/" + message.getTitle() + ".txt");
        FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(message.toString());
        bw.newLine();
        bw.close();
    }
}
