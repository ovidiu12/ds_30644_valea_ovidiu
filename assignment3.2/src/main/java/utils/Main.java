package main.java.utils;


import main.java.entities.ConsumerClass;
import main.java.entities.Dvd;
import main.java.entities.Producer;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

public class Main {

    private static final String HOST = "localhost";
    private static final int PORT = 8888;

    public Main() throws Exception{

        ConsumerClass consumer = new ConsumerClass("queue");
        Thread consumerThread = new Thread(consumer);
        consumerThread.start();

        Producer producer = new Producer("queue");
        for (int i = 0; i < 3; i++) {
            Dvd dvd = new Dvd("Dvd-ul "+ i, 2017, 12.12);
            producer.sendMessage(dvd);
            System.out.println("Message Number "+ i +" sent.");
            consumer.sendMail(dvd);
            TextDocumentService.saveToTextDocument(dvd);
        }
    }

    /**
     * @param args
     * @throws SQLException
     * @throws IOException
     */
    public static void main(String[] args) throws Exception{
        new Main();
    }
}
