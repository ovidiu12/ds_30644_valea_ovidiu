package main.java.servlets;

import main.java.entities.ConsumerClass;
import main.java.entities.Dvd;
import main.java.entities.Producer;
import main.java.utils.TextDocumentService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/dvdServlet")
public class MyServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String title = req.getParameter("title");
        String year = req.getParameter("year");
        String price = req.getParameter("price");

        Dvd dvd = new Dvd(title, Integer.parseInt(year), Double.parseDouble(price));
//        ConsumerClass consumer = new ConsumerClass("queue");
//        Thread consumerThread = new Thread(consumer);
//        consumerThread.start();

        Producer producer = new Producer("queue");
        producer.sendMessage(dvd);
        System.out.println("Message "+ dvd.getTitle() +" sent.");

        ConsumerClass.sendMail(dvd);
        TextDocumentService.saveToTextDocument(dvd);
    }

}
