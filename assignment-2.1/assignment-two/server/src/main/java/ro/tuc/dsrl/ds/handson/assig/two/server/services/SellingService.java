package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ISellingService;

import java.net.MalformedURLException;
import java.rmi.Naming; 
import java.rmi.RemoteException; 
import java.rmi.server.UnicastRemoteObject;
/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-two-server
 * @Since: Sep 1, 2015
 * @Description:
 * 	Class used for computation of taxes to be paid for a specific car. An instance
 * 	of this class is published in the Registry so that it can be remotely accessed
 * 	by a client.
 */
public class SellingService implements ISellingService {

	/*public SellingService() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	//private static final long serialVersionUID = 1L;

	public double computePrice(Car c) {
		// Dummy formula
		/*SellingService ss = new SellingService();
		Naming.rebind("SellingService", ss);*/
		if (c.getPurchasingPrice() <= 0) {
			throw new IllegalArgumentException("Price must be positive.");
		}
		return c.getPurchasingPrice() - (c.getPurchasingPrice()/7) * (2015-c.getYear());
	}
}
