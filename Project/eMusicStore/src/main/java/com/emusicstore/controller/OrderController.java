package com.emusicstore.controller;

import com.emusicstore.controller.admin.MailService;
import com.emusicstore.model.Cart;
import com.emusicstore.model.CartItem;
import com.emusicstore.model.Customer;
import com.emusicstore.model.CustomerOrder;
import com.emusicstore.service.CartService;
import com.emusicstore.service.CustomerOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class OrderController {

    @Autowired
    private CartService cartService;

    @Autowired
    private CustomerOrderService customerOrderService;

    @RequestMapping("/order/{cartId}")
    public String createOrder(@PathVariable(value = "cartId") int cartId) {
        CustomerOrder customerOrder = new CustomerOrder();
        Cart cart = cartService.getCartById(cartId);
        customerOrder.setCart(cart);


        Customer customer = cart.getCustomer();
        customerOrder.setCustomer(customer);
        String email = customer.getCustomerEmail();
        List<CartItem> cartItems = cart.getCartItems();
        String items="";

        for (int i=0; i<cartItems.size(); i++){
             items += "Product " + i+1 + " : " + cartItems.get(i).getProduct().getProductName() +"\n";
        }

        MailService mailService = new MailService("valea.ovidiu","uhuoackyiotvyrwx");
        mailService.sendMail(email,"Your order!",items + cart.toString());

        customerOrder.setBillingAddress(customer.getBillingAddress());
        customerOrder.setShippingAddress(customer.getShippingAddress());

        customerOrderService.addCustomerOrder(customerOrder);
        return "redirect:/checkout?cartId="+cartId;
    }
}
