<%--
  Created by IntelliJ IDEA.
  User: Ovi
  Date: 12/29/2017
  Time: 2:20 PM
  To change this template use File | Settings | File Templates.
--%>
<%@include file="/WEB-INF/views/template/header.jsp" %>

<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>About Us</h1>
            <p>This website is a project made for the Distributed Systems class from Tehnical University of Cluj-Napoca</p>
        </div>
        <img src="<c:url value="/resources/images/showroom.jpg" />" alt="showroom">
    </div>
</div>

<%@include file="/WEB-INF/views/template/footer.jsp" %>
