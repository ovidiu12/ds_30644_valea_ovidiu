package ds.assign12.servlets;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import ds.assign12.Assignment12.dao.FlightDAO;

/**
 * Servlet implementation class deleteFlightServlet
 */
@WebServlet("/deleteFlight")
public class DeleteFlightServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FlightDAO flightDAO;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteFlightServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		flightDAO=new FlightDAO(sf);
		String flightNumber=request.getParameter("fn");
		boolean success=flightDAO.deleteFlight(flightNumber);
		ServletContext sc = getServletContext();
		if(success) {
		sc.getRequestDispatcher("/operationSuccesfull.html").forward(request, response);}
		else {
			sc.getRequestDispatcher("/unexpected.html").forward(request, response);
		}

	
	}
}
