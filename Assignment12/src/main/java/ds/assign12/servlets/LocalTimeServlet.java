package ds.assign12.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import ds.assign12.Assignment12.dao.FlightDAO;
import ds.assign12.Assignment12.entities.City;
import ds.assign12.Assignment12.entities.Flight;

/**
 * Servlet implementation class LocalTimeServlet
 */
@WebServlet("/localTime")
public class LocalTimeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FlightDAO flightDAO;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LocalTimeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String flightNumber = request.getParameter("fn");
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		flightDAO = new FlightDAO(sf);
		Flight f = flightDAO.findFlightByNumber(flightNumber);
		City departure = f.getDepartureCity();
		City arrival = f.getArrivalCity();
		double latitudeDeparture = departure.getLatitude();
		double longitudeDeparture = departure.getLongitude();
		double latitudeArrival = arrival.getLatitude();
		double longitudeArrival = arrival.getLongitude();
		String urlDeparture = "http://new.earthtools.org/timezone/" + latitudeDeparture + "/" + longitudeDeparture;
		String urlArrival = "http://new.earthtools.org/timezone/" + latitudeArrival + "/" + longitudeArrival;
		URL u1 = new URL(urlDeparture);
		URL u2 = new URL(urlArrival);
		String timeDeparture = "";
		String timeArrival = "";

		URLConnection yc = u1.openConnection();
		URLConnection yc2 = u2.openConnection();
		yc.getInputStream();

		BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
		String inputLine;
		while ((inputLine = in.readLine()) != null) {
			System.out.println(inputLine);
			if (inputLine.contains("<localtime>")) {
				timeDeparture = inputLine;
				break;
			}
		}
		in.close();

		BufferedReader in2 = new BufferedReader(new InputStreamReader(yc2.getInputStream()));
		String inputLine2;
		while ((inputLine2 = in2.readLine()) != null) {
			System.out.println(inputLine2);
			if (inputLine2.contains("<localtime>")) {
				timeArrival = inputLine2;
				break;
			}
		}
		in.close();
		
		String finalDateDeparture=processDate(timeDeparture);
		String finalDateArrival=processDate(timeArrival);
		request.setAttribute("dateD", finalDateDeparture);
		request.setAttribute("dateA", finalDateArrival);
		request.getRequestDispatcher("localTime.jsp").forward(request, response);

	}

	
	private String processDate(String date) {
		return date.substring(13, 33);
	}
}
