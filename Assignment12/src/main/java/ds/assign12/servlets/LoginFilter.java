package ds.assign12.servlets;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import ds.assign12.Assignment12.dao.UserDAO;
import ds.assign12.Assignment12.dto.UserDTO;

@WebFilter(filterName = "loginFilter", urlPatterns = { "*.html", "*.jsp" })
public class LoginFilter implements Filter {
	private UserDAO userDAO;
	public static final String USERNAME = "username";
	public static final String USERS_PAGE = "/users";
	public static final String ADMINS_PAGE = "/a12/admins.html";
	public static final String ADMINS_ADD = "/a12/addFlight.html";
	public static final String ADMINS_DELETE = "/a12/deleteFlight.html";
	public static final String ADMINS_UPDATE = "/a12/updateFlight.html";
	public static final String ADMINS_VIEW = "/a12/viewFlight.jsp";
	public static final String LOGIN_PAGE = "/a12/login.html";
	public static final String LOGIN_PAGE2 = "/a12/";

	public void destroy() {
		// TODO Auto-generated method stub

	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String username = null;
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		HttpSession httpSession = httpRequest.getSession(true);
		String requestUrl = httpRequest.getRequestURI();
		boolean isUserLoggedIn = httpSession != null && httpSession.getAttribute(USERNAME) != null;

		//get username if user is logged in
		if (isUserLoggedIn) {
			username = httpSession.getAttribute(USERNAME).toString();
		}

		//not log in, redirect to log in
		if (loginVerification(requestUrl, isUserLoggedIn)) {
			chain.doFilter(request, response);
		} else {
			httpResponse.sendRedirect(httpRequest.getContextPath() + LOGIN_PAGE);
		}
		
		//user is logged in
		//redirect to default page based on role
     if((pageIsAccessed(LOGIN_PAGE, requestUrl) || pageIsAccessed( LOGIN_PAGE2, requestUrl))
    		&& isUserLoggedIn) {
    	 String role=userDAO.findUserByUsername(username).getRole();
    	 if(role.equals("ADMIN")) {
    		 httpResponse.sendRedirect(httpRequest.getContextPath() + ADMINS_PAGE);
    	 }
    	 else if(role.equals("USER")) {
    		 httpResponse.sendRedirect(httpRequest.getContextPath() + USERS_PAGE);
    	 }
     }
     //check permission for every page
		if ((userDoesnotHavePermission(username, "ADMIN") && adminPagesAreAccesed(requestUrl))
				|| (userDoesnotHavePermission(username, "USER") && userPagesAccessed(requestUrl))) {
			httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		} else {

			chain.doFilter(request, response);
		}

	}

	//returns true if user_page is accessed
	private boolean userPagesAccessed(String requestUrl) {

		return pageIsAccessed(USERS_PAGE, requestUrl);
	}

	private boolean adminPagesAreAccesed(String requestUrl) {
		return pageIsAccessed(ADMINS_PAGE, requestUrl) || pageIsAccessed(ADMINS_ADD, requestUrl)
				|| pageIsAccessed(ADMINS_DELETE, requestUrl) || pageIsAccessed(ADMINS_UPDATE, requestUrl)
				|| pageIsAccessed(ADMINS_VIEW, requestUrl);
	}

	private boolean userDoesnotHavePermission(String username2, String string) {
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		userDAO = new UserDAO(sf);
		UserDTO userLogged = userDAO.findUserByUsername(username2);
		return !userLogged.getRole().equals(string);

	}

	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

	private boolean pageIsAccessed(String pageName, String requestUrl) {
		return requestUrl.equals(pageName);
	}

	private boolean loginVerification(String requestUrl, boolean isUserLoggedIn) {
		return isUserLoggedIn || pageIsAccessed(LOGIN_PAGE, requestUrl);
	}

}
