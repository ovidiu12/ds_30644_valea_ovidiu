package ds.assign12.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import ds.assign12.Assignment12.dao.FlightDAO;
import ds.assign12.Assignment12.dto.FlightDTO;

/**
 * Servlet implementation class ViewFlightServlet
 */
@WebServlet("/viewFlight")
public class ViewFlightServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FlightDAO flightDAO;
    
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewFlightServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String value=request.getParameter("value");
		SessionFactory sf=new Configuration().configure().buildSessionFactory();
		flightDAO = new FlightDAO(sf);
		List<FlightDTO> allflights=flightDAO.findAllFlights();
		List<FlightDTO> flights=new ArrayList<FlightDTO>();
		if(allflights!=null)
		for(int  i=0;i<allflights.size();i++) {
			if(allflights.get(i).getArrival().getName().equalsIgnoreCase(value)||allflights.get(i).getDeparture().getName().equals(value)
					|| allflights.get(i).getAirplaneType().equals(value) || allflights.get(i).getFlightNumber().equals(value)) {
				if(!flights.contains(allflights.get(i))) {
					flights.add(allflights.get(i));
				}
			}
		}
		 PrintWriter out = response.getWriter();
	      out.println("<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n" +
	    	         "<html>\n" +
	    	         "<head><title>View Flight</title></head>\n"+
	    	         "<body bgcolor = \"#f0f0f0\">\n" +"<table>"+
	    	         "<tr><td>Flight number</td><td>Airplane type</td><td>City departure name</td><td>City arrival name</td><td>Departure time</td> <td>Arrival time</td>"
	    	        
	    	      );
	      
	      for(int i=0;i<flights.size();i++) {
	    	  out.println("<tr><td>"+flights.get(i).getFlightNumber()+"</td><td>"+flights.get(i).getAirplaneType()
	    			  +"</td><td>"+flights.get(i).getDeparture().getName()+"</td><td>"+flights.get(i).getArrival().getName()+"</td><td>"+flights.get(i).getDepartureTime()+"</td><td>"+flights.get(i).getArrivalTime()+"</td></tr>");
	      }
	      out.println("<table></body></html>");
	      
	}

}
