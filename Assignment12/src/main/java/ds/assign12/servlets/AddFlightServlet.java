package ds.assign12.servlets;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import ds.assign12.Assignment12.dao.CityDAO;
import ds.assign12.Assignment12.dao.FlightDAO;
import ds.assign12.Assignment12.dto.FlightDTO;
import ds.assign12.Assignment12.entities.City;

/**
 * Servlet implementation class AddFlightServlet
 */
@WebServlet("/addFlight")
public class AddFlightServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private CityDAO cityDAO;
	private FlightDAO flightDAO;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddFlightServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		cityDAO = new CityDAO(sf);
		flightDAO=new FlightDAO(sf);
		String flightNumber = request.getParameter("fn");
		String airplaneType = request.getParameter("at");
		String departureCityName = request.getParameter("dc");
		String arrivalCityName = request.getParameter("ac");
		City departureCity = cityDAO.findCityEntityByName(departureCityName);
		City arrivalCity = cityDAO.findCityEntityByName(arrivalCityName);
		FlightDTO newFlight = new FlightDTO();
		newFlight.setAirplaneType(airplaneType);
		newFlight.setArrival(arrivalCity);
		newFlight.setArrivalTime(request.getParameter("atd") + " " + request.getParameter("att"));
		newFlight.setDeparture(departureCity);
		newFlight.setDepartureTime(request.getParameter("dd") + " " + request.getParameter("dt"));
		newFlight.setFlightNumber(flightNumber);
		boolean success=flightDAO.addFlight(newFlight);
		ServletContext sc = getServletContext();
		if(success) {
		sc.getRequestDispatcher("/operationSuccesfull.html").forward(request, response);}
		else {
			sc.getRequestDispatcher("/unexpected.html").forward(request, response);
		}

	}

}
