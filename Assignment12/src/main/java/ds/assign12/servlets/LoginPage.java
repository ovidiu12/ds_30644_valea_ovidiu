package ds.assign12.servlets;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import ds.assign12.Assignment12.dao.UserDAO;
import ds.assign12.Assignment12.dto.UserDTO;

/**
 * Servlet implementation class LoginPage
 */
@WebServlet("/login")
public class LoginPage extends HttpServlet {

	private UserDAO userDAO;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginPage() {
    	
    }


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionFactory sf=new Configuration().configure().buildSessionFactory();

		userDAO = new UserDAO(sf);
		String username=request.getParameter("u");
		String pass=request.getParameter("p");
		UserDTO userFromDB=userDAO.findUserByUsername(username);
		if(userFromDB==null || !userFromDB.getPassword().equals(pass)) {
			ServletContext sc = getServletContext();
			sc.getRequestDispatcher("/error.html").forward(request, response);
		}
		else {
			ServletContext sc = getServletContext();
			HttpSession session = request.getSession(true);
	        session.setAttribute("username", username);       
	        session.setAttribute("password", pass);  
			if(checkPermissionUser(userFromDB,"USER"))
			sc.getRequestDispatcher("/users.jsp").forward(request, response);
			
			if(checkPermissionUser(userFromDB,"ADMIN"))
				sc.getRequestDispatcher("/admins.html").forward(request, response);
		}
     
	}


	private boolean checkPermissionUser(UserDTO userFromDB, String role) {
		return userFromDB.getRole().equals(role);
	}

}