package ds.assign12.Assignment12.dao;

import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import ds.assign12.Assignment12.dto.FlightDTO;
import ds.assign12.Assignment12.dto.mapper.FlightDTOMapper;
import ds.assign12.Assignment12.entities.Flight;

public class FlightDAO {
	private SessionFactory sf;

	public FlightDAO(SessionFactory sf) {
		// TODO Auto-generated constructor stub
		this.sf=sf;
	}
	private FlightDTOMapper flightMapper = new FlightDTOMapper();
	public List<FlightDTO>findAllFlights() {
	Session session = sf.openSession();
	Transaction tx = null;
	try {
		tx = session.beginTransaction();
		String hql = "FROM Flight";
		Query<?> query = session.createQuery(hql);
		
		if( !query.getResultList().isEmpty()) {
	
			List<Flight> flights = (List<Flight>) query.getResultList();
			return flights.stream().map(flight -> flightMapper.mapToDTO(flight)).collect(Collectors.toList());
		}

	} catch (HibernateException e) {
		if (tx != null)
			tx.rollback();
		e.printStackTrace();
	} finally {
		session.close();
	}

	return null;

}
	public boolean addFlight(FlightDTO flight) {
		Session session = sf.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Flight entity=flightMapper.mapToEntity(flight);
			session.save(entity);
			tx.commit();
			return true;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return false;
	}
	
	public boolean deleteFlight(String flightNumber) {
		Session session = sf.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Flight flight=findFlightByNumber(flightNumber);
			if(flight!=null)
			session.remove(flight);
			tx.commit();
			return true;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		return false;

	}

	public Flight findFlightByNumber(String flightNumber) {
		Session session = sf.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			String hql = "FROM Flight";
			Query<?> query = session.createQuery(hql);	
				List<Flight> flights = (List<Flight>) query.getResultList();
				if(!flights.isEmpty())
				{
					return flights.get(0);
				}
			

		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}
	
	public boolean updateFlight(FlightDTO flight) {
		Session session = sf.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Flight entity=findFlightByNumber(flight.getFlightNumber());
			if(flight.getAirplaneType() !="")
				entity.setAirplaneType(flight.getAirplaneType());
			if(flight.getArrival() !=null)
				entity.setArrivalCity(flight.getArrival());
			if(flight.getDeparture() !=null)
				entity.setDepartureCity(flight.getDeparture());
			if(flight.getArrivalTime()!="")
				entity.setArrivalTime(flight.getArrivalTime());
			if(flight.getDepartureTime()!="")
				entity.setDepartureTime(flight.getDepartureTime());
			session.update(entity);
			tx.commit();
			return true;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		return false;

	}
	
}
