package ds.assign12.Assignment12.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import ds.assign12.Assignment12.dto.CityDTO;
import ds.assign12.Assignment12.dto.mapper.CityDTOMapper;
import ds.assign12.Assignment12.entities.City;

public class CityDAO {
	private SessionFactory sf;
	private CityDTOMapper cityMapper = new CityDTOMapper();
	public CityDAO(SessionFactory sf) {
		// TODO Auto-generated constructor stub
		this.sf=sf;
	}
	
	public CityDTO findCityByName(String name) {
		Session session = sf.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			String hql = "FROM City c WHERE c.name = :name";
			Query<?> query = session.createQuery(hql);
			query.setParameter("name", name);
			if( !query.getResultList().isEmpty()) {
			return cityMapper.mapToDTO((City) query.getResultList().get(0));
			}

		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		return null;

	}
	
	public City findCityEntityByName(String name) {
		Session session = sf.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			String hql = "FROM City c WHERE c.name = :name";
			Query<?> query = session.createQuery(hql);
			query.setParameter("name", name);
			if( !query.getResultList().isEmpty()) {
			return (City) query.getResultList().get(0);
			}

		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		return null;

	}
}
