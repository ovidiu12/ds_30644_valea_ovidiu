package ds.assign12.Assignment12.dao;

import java.io.Serializable;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import ds.assign12.Assignment12.dto.UserDTO;
import ds.assign12.Assignment12.dto.mapper.UserDTOMapper;
import ds.assign12.Assignment12.entities.User;
public class UserDAO implements Serializable{

	
	private static final long serialVersionUID = 5L;
	private SessionFactory factory;

	public UserDAO(SessionFactory factory) {
		this.factory = factory;
	}

	UserDTOMapper userMapper = new UserDTOMapper();

	public UserDTO findUserByUsername(String username) {
	   	Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			String hql = "FROM User u WHERE u.username = :username";
			Query<?> query = session.createQuery(hql);
			query.setParameter("username", username);
			if( !query.getResultList().isEmpty()) {
			return userMapper.mapToDTO((User) query.getResultList().get(0));
			}

		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		return null;

	}

}
