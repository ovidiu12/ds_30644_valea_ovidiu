package ds.assign12.Assignment12.dto.mapper;

import java.io.Serializable;

import ds.assign12.Assignment12.dto.UserDTO;
import ds.assign12.Assignment12.entities.User;

public class UserDTOMapper implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public User mapToEntity(UserDTO dto) {
		User user=new User();
		user.setName(dto.getName());
		user.setPassword(dto.getPassword());
		user.setUsername(dto.getUsername());
		user.setRole(dto.getRole());

		return user;
	}
	
	public UserDTO mapToDTO(User entity) {
		UserDTO user=new UserDTO();
		user.setName(entity.getName());
		user.setPassword(entity.getPassword());
		user.setUsername(entity.getUsername());
		user.setRole(entity.getRole());
		user.setId(entity.getId());
		return user;
		
		
	
}
}
