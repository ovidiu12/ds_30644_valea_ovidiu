package ds.assign12.Assignment12.dto;

import ds.assign12.Assignment12.entities.City;

public class FlightDTO {
	private int id;
	private String flightNumber;
	private String airplaneType;
	private City departure;
	private String departureTime;
	private City arrival;
	private String arrivalTime;
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getAirplaneType() {
		return airplaneType;
	}
	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}
	public City getDeparture() {
		return departure;
	}
	public void setDeparture(City departure) {
		this.departure = departure;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public City getArrival() {
		return arrival;
	}
	public void setArrival(City arrivalCity) {
		this.arrival = arrivalCity;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}
