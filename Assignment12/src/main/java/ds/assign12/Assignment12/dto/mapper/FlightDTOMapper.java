package ds.assign12.Assignment12.dto.mapper;

import ds.assign12.Assignment12.dto.FlightDTO;
import ds.assign12.Assignment12.entities.Flight;

public class FlightDTOMapper {

	CityDTOMapper cityMapper=new CityDTOMapper();
	public Flight mapToEntity(FlightDTO dto) {
		Flight flight=new Flight();
		flight.setAirplaneType(dto.getAirplaneType());
		flight.setArrivalCity(dto.getArrival());
		flight.setArrivalTime(dto.getArrivalTime());
		flight.setDepartureCity(dto.getDeparture());
		flight.setDepartureTime(dto.getDepartureTime());
		flight.setFlightNumber(dto.getFlightNumber());
		return flight;
	}
	
	public FlightDTO mapToDTO(Flight entity) {
		FlightDTO flight=new FlightDTO();
		flight.setAirplaneType(entity.getAirplaneType());
		flight.setArrival(entity.getArrivalCity());
		flight.setArrivalTime(entity.getArrivalTime());
		flight.setDeparture(entity.getDepartureCity());
		flight.setDepartureTime(entity.getDepartureTime());
		flight.setFlightNumber(entity.getFlightNumber());
		flight.setId(entity.getId());
		return flight;
		
		
	}

}
