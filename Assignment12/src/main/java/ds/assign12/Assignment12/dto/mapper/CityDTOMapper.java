package ds.assign12.Assignment12.dto.mapper;

import ds.assign12.Assignment12.dto.CityDTO;
import ds.assign12.Assignment12.entities.City;

public class CityDTOMapper {

	public City mapToEntity(CityDTO dto) {
		City city=new City();
		city.setLatitude(dto.getLatitude());
		city.setLongitude(dto.getLongitude());
		city.setName(dto.getName());
		return city;
	}
	
	public CityDTO mapToDTO(City entity) {
		CityDTO city=new CityDTO();
		city.setLatitude(entity.getLatitude());
		city.setLongitude(entity.getLongitude());
		city.setName(entity.getName());
		city.setId(entity.getId());
		return city;
		
		
	}
}
