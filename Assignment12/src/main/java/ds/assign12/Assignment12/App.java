package ds.assign12.Assignment12;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import ds.assign12.Assignment12.entities.City;
import ds.assign12.Assignment12.entities.Flight;
import ds.assign12.Assignment12.entities.User;

/**
 * Hello world!
 *
 */
public class App {
	private static SessionFactory factory;

	public static void main(String[] args) {

		addCity();
		addUser();
	}

	private static void addUser() {
		 factory =new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(User.class).addAnnotatedClass(Flight.class).buildSessionFactory();
	    	Session session = factory.openSession();
	        Transaction tx = null;
	   
	        
	        try {
	           tx = session.beginTransaction();
	           User u=new User();
	           u.setName("Bianca");
	           u.setPassword("oparolagrea");
	           u.setUsername("bianca");
	      
	           u.setRole("ADMIN");
	           session.save(u);
	           tx.commit();
	           }
	           catch (HibernateException e) {
	               if (tx!=null) tx.rollback();
	               e.printStackTrace(); 
	            } finally {
	               session.close(); 
	            }

		}       

	public static void addCity() {
        factory = new Configuration().configure().addAnnotatedClass(City.class).buildSessionFactory();
    	Session session = factory.openSession();
        Transaction tx = null;
   
        
        try {
           tx = session.beginTransaction();
           City city=new City();
           city.setName("Cluj Napoca");
           city.setLatitude(46.770439);
           city.setLongitude(23.591423);
           session.save(city);
           tx.commit();
           }
           catch (HibernateException e) {
               if (tx!=null) tx.rollback();
               e.printStackTrace(); 
            } finally {
               session.close(); 
            }

	}       
}
