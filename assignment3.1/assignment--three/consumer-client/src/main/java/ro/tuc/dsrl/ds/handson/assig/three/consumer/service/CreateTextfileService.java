package ro.tuc.dsrl.ds.handson.assig.three.consumer.service;

import java.io.Writer;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.IOException;
public class CreateTextfileService {

    public void createFile(String content){
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("message.txt"), "utf-8"))) {
            writer.write(content);
        }
        catch (IOException e) {
            System.err.println("Problem writing to the file message.txt");
        }
    }
}
